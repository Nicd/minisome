# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

minisome_mime_type = "text/minisome"

config :minisome,
  mime_type: minisome_mime_type,
  ecto_repos: [Minisome.Storage.Repo]

# Configures the endpoints
config :minisome, Minisome.Web.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: Minisome.Web.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Minisome.PubSub,
  live_view: [signing_salt: "vfAnu01K"]

config :minisome, Minisome.API.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: Minisome.Web.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Minisome.PubSub

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.0",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :mime, :types, %{
  minisome_mime_type => ["minisome"]
}

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
