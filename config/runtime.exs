import Config
import Minisome.ConfigHelpers, only: [get_env: 1, get_env: 2, get_env: 3]

# config/runtime.exs is executed for all environments, including
# during releases. It is executed after compilation and before the
# system starts, so it is typically used to load production configuration
# and secrets from environment variables or elsewhere. Do not define
# any compile-time configuration in here, as it won't be applied.

config :minisome,
  # How often to fetch feeds of other hosts
  update_delay: get_env("UPDATE_DELAY", 10, :int)

# Start the phoenix server if environment is set and running in a release
if System.get_env("PHX_SERVER") && System.get_env("RELEASE_NAME") do
  config :minisome, Minisome.Web.Endpoint, server: true
end

host = get_env("PHX_HOST", "localhost")
port = get_env("WEB_PORT", 4000, :int)
api_port = get_env("API_PORT", 33101, :int)

cond do
  config_env() == :dev ->
    config :minisome, Minisome.Storage.Repo,
      database:
        get_env("DATABASE_PATH", Path.expand("../minisome_dev.db", Path.dirname(__ENV__.file)))

    config :minisome, Minisome.Web.Endpoint,
      # Binding to loopback ipv4 address prevents access from other machines.
      # Change to `ip: {0, 0, 0, 0}` to allow access from other machines.
      http: [ip: {127, 0, 0, 1}, port: port],
      check_origin: false,
      code_reloader: true,
      debug_errors: true,
      secret_key_base: "MR3TEt40ApczckU3+IRzAi91t5iNhbiRt4l0ChL30DdADwAwtdfa++i+NX0ezfc1",
      watchers: [
        # Start the esbuild watcher by calling Esbuild.install_and_run(:default, args)
        esbuild: {Esbuild, :install_and_run, [:default, ~w(--sourcemap=inline --watch)]}
      ]

    config :minisome, Minisome.API.Endpoint,
      http: [ip: {127, 0, 0, 1}, port: api_port],
      check_origin: false,
      code_reloader: true,
      debug_errors: true,
      secret_key_base: "MR3TEt40ApczckU3+IRzAi91t5iNhbiRt4l0ChL30DdADwAwtdfa++i+NX0ezfc1"

  config_env() == :prod ->
    database_path =
      System.get_env("DATABASE_PATH") ||
        raise """
        environment variable DATABASE_PATH is missing.
        For example: /etc/minisome/minisome.db
        """

    config :minisome, Minisome.Storage.Repo,
      database: database_path,
      pool_size: get_env("POOL_SIZE", 5, :int)

    # The secret key base is used to sign/encrypt cookies and other secrets.
    # A default value is used in config/dev.exs and config/test.exs but you
    # want to use a different value for prod and you most likely don't want
    # to check this value into version control, so we use an environment
    # variable instead.
    secret_key_base =
      System.get_env("SECRET_KEY_BASE") ||
        raise """
        environment variable SECRET_KEY_BASE is missing.
        You can generate one by calling: mix phx.gen.secret
        """

    config :minisome, Minisome.Web.Endpoint,
      url: [host: host, port: 443],
      http: [
        # Enable IPv6 and bind on all interfaces.
        # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
        # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
        # for details about using IPv6 vs IPv4 and loopback vs public addresses.
        ip: {0, 0, 0, 0, 0, 0, 0, 0},
        port: port
      ],
      secret_key_base: secret_key_base

    config :minisome, Minisome.API.Endpoint,
      url: [host: host, port: api_port],
      http: [
        # Enable IPv6 and bind on all interfaces.
        # Set it to  {0, 0, 0, 0, 0, 0, 0, 1} for local network only access.
        # See the documentation on https://hexdocs.pm/plug_cowboy/Plug.Cowboy.html
        # for details about using IPv6 vs IPv4 and loopback vs public addresses.
        ip: {0, 0, 0, 0, 0, 0, 0, 0},
        port: api_port
      ],
      secret_key_base: secret_key_base

    # ## Using releases
    #
    # If you are doing OTP releases, you need to instruct Phoenix
    # to start each relevant endpoint:
    #
    #     config :minisome, Minisome.Web.Endpoint, server: true
    #
    # Then you can assemble a release by calling `mix release`.
    # See `mix help release` for more information.
end
