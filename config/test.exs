import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :minisome, Minisome.Storage.Repo,
  database: Path.expand("../minisome_test.db", Path.dirname(__ENV__.file)),
  pool_size: 5,
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :minisome, Minisome.Web.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "nlj8uUOi1lhYBQz7cO87ICbxJSAOoKEEqmf0qWJs3Gs2h4XyJKqzBpM6MFmM2nkG",
  server: false

config :minisome, Minisome.API.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 33101],
  secret_key_base: "nlj8uUOi1lhYBQz7cO87ICbxJSAOoKEEqmf0qWJs3Gs2h4XyJKqzBpM6MFmM2nkG",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
