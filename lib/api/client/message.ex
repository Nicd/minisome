defmodule Minisome.API.Client.Message do
  @moduledoc """
  Low level message handling tools.

  This lower level does not have an opinion on what the message contains. It is only concerned
  about a blob payload and its signature.
  """

  require Logger

  import Minisome.Utils.TypedStruct

  alias Minisome.Crypto.SSH

  defmodule RawMessage do
    deftypedstruct(
      %{
        data: binary(),
        signature: SSH.Signature.t()
      },
      "Low level message and its signature."
    )
  end

  @doc """
  Parse a binary into a RawMessage.

  The binary must have the OpenSSH style signature right at the start, followed by two newlines, then the payload after that.
  """
  @spec parse_message(binary()) :: RawMessage.t() | nil
  def parse_message(data) do
    with {:split, [sig_part, data_part]} <-
           {:split, :binary.split(data, "#{SSH.openssh_sig_end()}\n\n")},
         sig_data = sig_part <> SSH.openssh_sig_end(),
         {:ok, signature} <- SSH.parse_signature(sig_data) do
      %RawMessage{
        data: data_part,
        signature: signature
      }
    else
      {:error, error} ->
        Logger.warning("Unable to parse message due to: #{inspect(error)}")
        nil

      {:split, _} ->
        Logger.warning("Unable to parse message due to missing footer")
        nil
    end
  end

  @doc """
  Encode given data using the key pair into a raw message.
  """
  @spec encode_message(binary(), SSH.KeyPair.t()) :: RawMessage.t()
  def encode_message(data, keys) do
    signature = SSH.sign(data, keys)

    %RawMessage{
      data: data,
      signature: signature
    }
  end

  @doc """
  Format the raw message into a message string.
  """
  @spec format_message(RawMessage.t()) :: String.t()
  def format_message(%RawMessage{} = msg) do
    "#{SSH.format_signature(msg.signature)}\n\n#{msg.data}"
  end
end
