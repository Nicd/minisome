defmodule Minisome.API.Client.MessageLibraries.Auth do
  alias Minisome.API.Client.MessageLibraries.GenericLibrary
  alias Minisome.API.Client.ProtocolMessage
  alias Minisome.API.Client.MessageLibraries.Auth.KeyInfoMessage

  @behaviour GenericLibrary

  @message_mapping %{
    KeyInfoMessage.message_type() => KeyInfoMessage
  }

  @impl GenericLibrary
  @spec library_name() :: String.t()
  def library_name(), do: "auth"

  @impl GenericLibrary
  @spec encoded_version() :: Version.t()
  def encoded_version(),
    do: %Version{
      major: 1,
      minor: 0,
      patch: 0
    }

  @impl GenericLibrary
  @spec accepted_version() :: Version.t()
  def accepted_version(), do: encoded_version()

  @impl GenericLibrary
  @spec decode(ProtocolMessage.t()) :: {:ok, any()} | {:error, atom()}
  def decode(%ProtocolMessage{} = message) do
    GenericLibrary.generic_decode(@message_mapping, message)
  end
end
