defmodule Minisome.API.Client.MessageLibraries.Auth.KeyInfoMessage do
  import Minisome.Utils.TypedStruct

  alias Minisome.API.Client.MessageLibraries.GenericMessage
  alias Minisome.Crypto.SSH

  @behaviour GenericMessage

  defmodule Key do
    deftypedstruct(%{
      key: SSH.PublicKey.t(),
      expires: DateTime.t()
    })
  end

  deftypedstruct(%{
    keys: [Key.t()]
  })

  @impl GenericMessage
  @spec message_type() :: String.t()
  def message_type(), do: "key_info"

  @impl GenericMessage
  @spec encode(t()) :: {String.t(), map()}
  def encode(%__MODULE__{} = message) do
    keys = Enum.map(message.keys, &Map.from_struct/1)

    GenericMessage.encode(__MODULE__, %{
      "keys" => keys
    })
  end

  @impl GenericMessage
  @spec decode(any()) :: {:ok, t()} | :error
  def decode(payload) do
    with keys when is_list(keys) <- payload,
         true <- Enum.all?(keys, &payload_key_match?/1),
         {:ok, parsed_keys} <- parse_keys(keys) do
      {:ok,
       %__MODULE__{
         keys: parsed_keys
       }}
    else
      _ -> :error
    end
  end

  @spec payload_key_match?(any()) :: boolean()
  defp payload_key_match?(key_data)

  defp payload_key_match?(%{"key" => key, "expires" => expires})
       when is_binary(key) and is_struct(expires, DateTime),
       do: true

  defp payload_key_match?(_), do: false

  @spec parse_keys([map()]) :: {:ok, [Key.t()]} | :error
  defp parse_keys(keys) do
    reduced =
      Enum.reduce_while(keys, [], fn %{"key" => key, "expires" => expires}, acc ->
        case SSH.load_public_key(key) do
          {:ok, parsed_key} -> {:cont, [%Key{key: parsed_key, expires: expires} | acc]}
          :error -> {:halt, :error}
        end
      end)

    if reduced == :error do
      :error
    else
      {:ok, reduced}
    end
  end
end
