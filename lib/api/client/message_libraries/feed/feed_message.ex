defmodule Minisome.API.Client.MessageLibraries.Feed.FeedMessage do
  import Minisome.Utils.TypedStruct

  alias Minisome.API.Client.MessageLibraries.GenericMessage

  @behaviour GenericMessage

  deftypedstruct(%{
    contents: [Minisome.API.Client.Message.RawMessage.t()]
  })

  defmodule Decoded do
    deftypedstruct(%{
      contents: [struct()]
    })
  end

  @impl GenericMessage
  @spec message_type() :: String.t()
  def message_type(), do: "feed"

  @impl GenericMessage
  @spec encode(t()) :: {String.t(), map()}
  def encode(%__MODULE__{} = message) do
    GenericMessage.encode(__MODULE__, %{
      "contents" =>
        for content <- message.contents do
          Minisome.API.Client.Message.format_message(content)
        end
    })
  end

  @impl GenericMessage
  @spec decode(any()) :: {:ok, t()} | :error
  def decode(payload) do
    with true <- is_map(payload),
         {:ok, contents} when is_list(contents) <- Map.fetch(payload, "contents"),
         raw_contents <- Enum.map(contents, &Minisome.API.Client.Message.parse_message/1),
         true <- Enum.all?(raw_contents, &(!is_nil(&1))) do
      {:ok,
       %__MODULE__{
         contents: raw_contents
       }}
    else
      _ -> :error
    end
  end
end
