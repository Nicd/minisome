defmodule Minisome.API.Client.MessageLibraries.GenericLibrary do
  alias Minisome.API.Client.ProtocolMessage

  @type message_mapping() :: %{
          optional(Minisome.API.Client.MessageLibraries.GenericMessage.type()) => module()
        }

  @callback library_name() :: String.t()
  @callback encoded_version() :: Version.t()
  @callback accepted_version() :: Version.t()
  @callback decode(ProtocolMessage.t()) :: {:ok, any()} | {:error, atom()}

  def dispatch(message) do
  end

  @spec generic_decode(message_mapping(), ProtocolMessage.t()) :: {:ok, any()} | {:error, atom()}
  def generic_decode(message_mapping, %ProtocolMessage{} = message) do
    case Map.get(message_mapping, message.type) do
      nil -> {:error, :unknown_type}
      module -> module.decode(message.payload)
    end
  end

  @spec encode(module(), {String.t(), any()}) :: ProtocolMessage.t()
  def encode(module, {type, payload}) do
    %ProtocolMessage{
      library: module,
      type: type,
      version: module.encoded_version(),
      payload: payload
    }
  end
end
