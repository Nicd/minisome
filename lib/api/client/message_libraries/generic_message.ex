defmodule Minisome.API.Client.MessageLibraries.GenericMessage do
  @type type() :: String.t()

  @callback message_type() :: type()
  @callback encode(any()) :: {String.t(), any()}
  @callback decode(any()) :: {:ok, any()} | :error

  @spec encode(module(), any()) :: {String.t(), any()}
  def encode(module, payload) do
    {module.message_type, payload}
  end
end
