defmodule Minisome.API.Client.MessageLibraries.Post.PostMessage do
  import Minisome.Utils.TypedStruct

  alias Minisome.API.Client.MessageLibraries.GenericMessage

  @behaviour GenericMessage

  deftypedstruct(%{
    id: String.t(),
    text: String.t(),
    tags: [String.t()],
    posted: DateTime.t()
  })

  @impl GenericMessage
  @spec message_type() :: String.t()
  def message_type(), do: "post"

  @impl GenericMessage
  @spec encode(t()) :: {String.t(), map()}
  def encode(%__MODULE__{} = message) do
    GenericMessage.encode(__MODULE__, %{
      "id" => message.id,
      "text" => message.text,
      "tags" => message.tags,
      "posted" => message.posted
    })
  end

  @impl GenericMessage
  @spec decode(any()) :: {:ok, t()} | :error
  def decode(payload) do
    with true <- is_map(payload),
         {:ok, text} <- Map.fetch(payload, "text"),
         {:ok, tags} <- Map.fetch(payload, "tags"),
         {:ok, posted} <- Map.fetch(payload, "posted"),
         {:ok, id} <- Map.fetch(payload, "id"),
         true <- String.valid?(text),
         true <- is_struct(posted, DateTime),
         true <- is_list(tags),
         true <- String.valid?(id),
         true <- Enum.all?(tags, &String.valid?/1) do
      {:ok,
       %__MODULE__{
         id: id,
         text: text,
         tags: tags,
         posted: posted
       }}
    else
      _ -> :error
    end
  end
end
