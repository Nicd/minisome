defmodule Minisome.API.Client.ProtocolHandler do
  @moduledoc """
  Handler for protocol messages to/from raw messages.
  """

  require Logger

  import Minisome.Utils.WithHelper

  alias Minisome.API.Client.Message
  alias Minisome.API.Client.ProtocolMessage
  alias Minisome.API.Client.MessageLibraries.Auth
  alias Minisome.API.Client.MessageLibraries.Post
  alias Minisome.API.Client.MessageLibraries.Feed

  @library_mapping %{
    Auth.library_name() => Auth,
    Post.library_name() => Post,
    Feed.library_name() => Feed
  }

  @doc """
  Decode raw message into a protocol message.
  """
  @spec decode_raw(Message.RawMessage.t()) :: {:ok, ProtocolMessage.t()} | {:error, any()}
  def decode_raw(%Message.RawMessage{} = raw) do
    with {:ok, data} <- op(:unpack, Msgpax.unpack(raw.data)),
         {:destructure,
          %{"library" => library, "type" => type, "version" => version, "payload" => payload}} <-
           {:destructure, data},
         library_module <- op(:library_type, Map.get(@library_mapping, library), :permissive),
         {:version, [major_version, minor_version, patch_version]}
         when is_integer(major_version) and is_integer(minor_version) and
                is_integer(patch_version) <- {:version, version},
         true <- op(:type_type, is_binary(type)) do
      {:ok,
       %ProtocolMessage{
         library: library_module,
         type: type,
         version: %Version{
           major: major_version,
           minor: minor_version,
           patch: patch_version
         },
         payload: payload
       }}
    else
      {:unpack, err} ->
        Logger.warn("Unable to msgpack decode protocol message: #{inspect(err)}")
        {:error, {:invalid_msgpack, err}}

      {:destructure, payload} ->
        Logger.warn("Invalid protocol message payload: #{inspect(payload)}")
        {:error, :invalid_payload}

      {:library_type, type} ->
        Logger.warn("Invalid protocol message library: #{inspect(type)}")
        {:error, :invalid_library_type}

      {:version, version} ->
        Logger.warn("Invalid protocol message version: #{inspect(version)}")
        {:error, :invalid_version}

      {:type_type, _} ->
        {:error, :invalid_type_type}
    end
  end

  @doc """
  Dispatch the protocol message to its library for decoding.
  """
  @spec handle_message(ProtocolMessage.t()) :: {:ok, any()} | {:error, atom()}
  def handle_message(%ProtocolMessage{} = message) do
    message.library.decode(message)
  end

  @doc """
  Encode protocol message into payload binary that can be signed.
  """
  @spec encode_message(ProtocolMessage.t()) :: {:ok, binary()} | {:error, any()}
  def encode_message(%ProtocolMessage{} = message) do
    data = %{
      library: message.library.library_name(),
      type: message.type,
      version: [message.version.major, message.version.minor, message.version.patch],
      payload: message.payload
    }

    Msgpax.pack(data, iodata: false)
  end
end
