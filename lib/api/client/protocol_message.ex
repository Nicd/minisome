defmodule Minisome.API.Client.ProtocolMessage do
  @moduledoc """
  In Minisome, the message protocol defines messages as objects with certain properties:

  ```json
  {
    "library": "auth",
    "type": "key_info",
    "version": [1, 2, 1],
    "payload": {"keys": []}
  }
  ```

  * The **library** is a coherent grouping of messages into a set that has stand-alone value. For
  example the "auth" library contains messages related to authentication of nodes.
  * The **type** is a single message type within the library.
  * The **version** is used to prevent mistakes due to changed protocol versions. Each library is
    versioned separately.
  * The **payload** is specific to the library and message type.
  """

  import Minisome.Utils.TypedStruct

  deftypedstruct(
    %{
      library: module(),
      type: binary(),
      version: Version.t(),
      payload: any()
    },
    """
    A protocol message belonging to a message library.
    """
  )
end
