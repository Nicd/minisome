defmodule Minisome.API.AuthController do
  use Phoenix.Controller, namespace: Minisome.API
  import Plug.Conn

  alias Minisome.Crypto.SSH
  alias Minisome.Storage.Auth.MyKey
  alias Minisome.API.Client.MessageLibraries.Auth

  @spec key_info(Plug.Conn.t(), Plug.Conn.params()) :: Plug.Conn.t()
  def key_info(conn, _params) do
    keys = MyKey.get_active_keys()

    transformed_keys =
      Enum.map(keys, fn {%SSH.KeyPair{} = key, expires} ->
        %Auth.KeyInfoMessage.Key{key: key.public, expires: expires}
      end)

    message = %Auth.KeyInfoMessage{keys: transformed_keys}
    payload = Auth.KeyInfoMessage.encode(message)
    protocol_message = Minisome.API.Client.MessageLibraries.GenericLibrary.encode(Auth, payload)
    {:ok, packed} = Minisome.API.Client.ProtocolHandler.encode_message(protocol_message)
    raw = Minisome.API.Client.Message.encode_message(packed, keys |> List.first() |> elem(0))
    data = Minisome.API.Client.Message.format_message(raw)

    conn
    |> send_resp(200, data)
  end
end
