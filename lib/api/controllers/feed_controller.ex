defmodule Minisome.API.FeedController do
  use Phoenix.Controller, namespace: Minisome.API
  import Plug.Conn

  alias Minisome.API.Client.MessageLibraries.Post, as: PostLibrary
  alias Minisome.API.Client.MessageLibraries.Post.PostMessage
  alias Minisome.API.Client.MessageLibraries.Feed, as: FeedLibrary
  alias Minisome.API.Client.MessageLibraries.Feed.FeedMessage
  alias Minisome.Some.Post.API, as: PostAPI

  @spec feed(Plug.Conn.t(), Plug.Conn.params()) :: Plug.Conn.t()
  def feed(conn, _params) do
    keys = Minisome.Storage.Auth.MyKey.get_active_keys()

    posts = PostAPI.all()

    post_messages =
      for post <- posts do
        %PostMessage{
          id: post.id,
          text: post.text,
          tags: post.tags,
          posted: post.posted
        }
        |> form_post_message(keys)
      end

    message = %FeedMessage{
      contents: post_messages
    }

    payload = FeedMessage.encode(message)

    protocol_message =
      Minisome.API.Client.MessageLibraries.GenericLibrary.encode(FeedLibrary, payload)

    {:ok, packed} = Minisome.API.Client.ProtocolHandler.encode_message(protocol_message)
    raw = Minisome.API.Client.Message.encode_message(packed, keys |> List.first() |> elem(0))
    data = Minisome.API.Client.Message.format_message(raw)

    conn
    |> send_resp(200, data)
  end

  defp form_post_message(post, keys) do
    payload = PostMessage.encode(post)

    protocol_message =
      Minisome.API.Client.MessageLibraries.GenericLibrary.encode(PostLibrary, payload)

    {:ok, packed} = Minisome.API.Client.ProtocolHandler.encode_message(protocol_message)
    Minisome.API.Client.Message.encode_message(packed, keys |> List.first() |> elem(0))
  end
end
