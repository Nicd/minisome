defmodule Minisome.API.ContentTypePlug do
  @behaviour Plug

  @minisome_content_type Application.compile_env!(:minisome, :mime_type)

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  @spec call(Plug.Conn.t(), any) :: Plug.Conn.t()
  def call(conn, _opts) do
    Plug.Conn.put_resp_content_type(conn, @minisome_content_type)
  end
end
