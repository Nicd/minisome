defimpl Msgpax.Packer, for: Minisome.Crypto.SSH.PublicKey do
  @spec pack(Minisome.Crypto.SSH.PublicKey.t()) :: iodata()
  def pack(%Minisome.Crypto.SSH.PublicKey{} = key) do
    <<
      type_len::unsigned-integer-32,
      type::binary-size(type_len),
      key_len::unsigned-integer-32,
      str_key::binary-size(key_len)
    >> = :ssh_file.encode(key.data, :ssh2_pubkey)

    "#{type} #{str_key}"
  end
end
