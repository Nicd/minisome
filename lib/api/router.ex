defmodule Minisome.API.Router do
  use Phoenix.Router

  import Plug.Conn
  import Phoenix.Controller

  pipeline :api do
    plug :accepts, ["minisome"]
    plug Minisome.API.ContentTypePlug
  end

  scope "/", Minisome.API do
    pipe_through :api

    get "/key-info", AuthController, :key_info

    get "/feed", FeedController, :feed
  end
end
