defmodule Minisome.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Finch, name: Minisome.Some.HTTP},

      # Start the Ecto repository
      Minisome.Storage.Repo,
      # Start the Telemetry supervisor
      Minisome.Web.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Minisome.PubSub},
      # Start the Endpoint (http/https)
      Minisome.Web.Endpoint,
      Minisome.API.Endpoint,
      # Start a worker by calling: Minisome.Worker.start_link(arg)
      # {Minisome.Worker, arg}

      Supervisor.child_spec({Phoenix.PubSub, name: Minisome.Some.Fetcher.PubSub},
        id: Minisome.Some.Fetcher.PubSub
      ),
      {Minisome.Some.Fetcher,
       %Minisome.Some.Fetcher.Options{
         hosts_getter: &Minisome.Some.Host.API.all/0,
         fetch_delay: Application.get_env(:minisome, :update_delay) * 1_000
       }}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Minisome.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    Minisome.Web.Endpoint.config_change(changed, removed)
    :ok
  end
end
