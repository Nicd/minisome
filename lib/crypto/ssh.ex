defmodule Minisome.Crypto.SSH do
  @moduledoc """
  Implementation of SSH signing and verifying in Elixir / OTP 24.

  NOTE: Only supports ED25519 keys.
  """

  import Minisome.Utils.TypedStruct

  @default_namespace "minisome"
  @default_hash :sha512
  @reserved_data ""
  @magic_preamble "SSHSIG"
  @sig_version 1

  @openssh_sig_start "-----BEGIN SSH SIGNATURE-----"
  @openssh_sig_end "-----END SSH SIGNATURE-----"
  @openssh_sig_line_length 76

  @typedoc """
  Algorithm used for hashing in the signature.
  """
  @type hash_algo :: :sha256 | :sha512

  defmodule PrivateKey do
    deftypedstruct(%{
      data: :public_key.private_key()
    })
  end

  defmodule PublicKey do
    deftypedstruct(%{
      data: :public_key.public_key()
    })
  end

  defmodule KeyPair do
    deftypedstruct(%{
      private: PrivateKey.t(),
      public: PublicKey.t()
    })
  end

  defmodule Signature do
    deftypedstruct(%{
      signature: binary(),
      namespace: binary(),
      hash: Minisome.Crypto.SSH.hash_algo(),
      keys: [PublicKey.t()]
    })
  end

  @doc """
  Load SSH key pair from given binary.

  The binary should be in OpenSSH private key format. There should only be one private/public key
  pair in the binary.
  """
  @spec load_key_pair(binary()) :: {:ok, KeyPair.t()} | :error
  def load_key_pair(data) do
    case :ssh_file.decode(data, :public_key) do
      [{{:ed_pri, _, _, _} = privk, _priv_meta}, {{:ed_pub, _, _} = pubk, _pub_meta}] ->
        {:ok, %KeyPair{public: %PublicKey{data: pubk}, private: %PrivateKey{data: privk}}}

      _ ->
        :error
    end
  end

  @doc """
  Load public key from given binary.

  The binary should be in the OpenSSH public key format. There should only be one public key in
  the binary.
  """
  @spec load_public_key(binary()) :: {:ok, PublicKey.t()} | :error
  def load_public_key(data) do
    case :ssh_file.decode(data, :public_key) do
      [{{:ed_pub, _, _} = pubk, _pub_meta}] ->
        {:ok, %PublicKey{data: pubk}}

      _ ->
        :error
    end
  end

  @doc """
  Sign data with key pair.
  """
  @spec sign(binary(), KeyPair.t(), binary(), hash_algo()) :: Signature.t()
  def sign(data, %KeyPair{} = keys, namespace \\ @default_namespace, hash \\ @default_hash) do
    payload = form_payload(data, namespace, hash)
    signature = :public_key.sign(payload, hash, keys.private.data)

    %Signature{
      signature: signature,
      namespace: namespace,
      hash: hash,
      keys: [keys.public.data]
    }
  end

  @doc """
  Verify signed data using the signature and public key of the signer.

  The keys embedded in the signature are ignored.
  """
  @spec verify(binary(), Signature.t(), PublicKey.t()) :: boolean()
  def verify(
        data,
        %Signature{signature: signature, namespace: namespace, hash: hash},
        %PublicKey{data: key}
      ) do
    payload = form_payload(data, namespace, hash)
    :public_key.verify(payload, hash, signature, key)
  end

  @doc """
  Create SSH style signature string from given signature data.
  """
  @spec format_signature(Signature.t()) :: String.t()
  def format_signature(%Signature{
        signature: signature,
        namespace: namespace,
        hash: hash,
        keys: [key | _]
      }) do
    key_part = ssh_string(pub_key_to_ssh_name(key)) <> ssh_string(pub_key_extract(key))
    sig_part = ssh_string(pub_key_to_ssh_name(key)) <> ssh_string(signature)

    blob =
      @magic_preamble <>
        <<@sig_version::unsigned-integer-32>> <>
        ssh_string(key_part) <>
        ssh_string(namespace) <>
        ssh_string(@reserved_data) <>
        ssh_string(hash |> Atom.to_string()) <>
        ssh_string(sig_part)

    blob_str = Base.encode64(blob)
    blob_size = byte_size(blob_str)

    blob_split =
      Enum.reduce_while(0..blob_size//@openssh_sig_line_length, "", fn index, acc ->
        length = min(@openssh_sig_line_length, blob_size - index)
        part = :binary.part(blob_str, index, length)
        next_acc = acc <> part <> "\n"

        if index + @openssh_sig_line_length < blob_size do
          {:cont, next_acc}
        else
          {:halt, next_acc}
        end
      end)

    "#{@openssh_sig_start}\n" <>
      blob_split <>
      "#{@openssh_sig_end}"
  end

  @doc """
  Parse signature data from OpenSSH signature format.
  """
  @spec parse_signature(binary()) :: {:ok, Signature.t()} | {:error, atom()}
  def parse_signature(signature) do
    signature = :binary.replace(signature, "\n", "", [:global])
    inner_start = byte_size(@openssh_sig_start)
    inner_end = byte_size(signature) - byte_size(@openssh_sig_start) - byte_size(@openssh_sig_end)
    data = :binary.part(signature, inner_start, inner_end)

    with {:base64, {:ok, decoded}} <- {:base64, Base.decode64(data)},
         {:ok, after_preamble} <- parse_preamble(decoded),
         {:ok, public_keys, after_keys} <- parse_keys(after_preamble),
         {:ok, namespace, after_namespace} <- parse_namespace(after_keys),
         {:ok, after_reserved} <- parse_reserved(after_namespace),
         {:ok, hash, after_hash} <- parse_hash(after_reserved),
         {:ok, signature} <- parse_signature_part(after_hash) do
      {:ok,
       %Signature{
         signature: signature,
         namespace: namespace,
         hash: hash,
         keys: public_keys
       }}
    else
      {:base64, _} -> {:error, :base64}
      error -> error
    end
  end

  @doc """
  Form payload for signing according to the OpenSSH signature format.
  """
  @spec form_payload(binary(), binary(), hash_algo()) :: binary()
  def form_payload(data, namespace, hash) do
    digest = :crypto.hash(hash, data)

    @magic_preamble <>
      ssh_string(namespace) <>
      ssh_string(@reserved_data) <>
      ssh_string(hash |> Atom.to_string()) <>
      ssh_string(digest)
  end

  @doc """
  Convert given binary to OpenSSH signature "string" type, i.e. a binary blob prefixed with an
  unsigned 32 bit integer length.
  """
  @spec ssh_string(binary()) :: <<_::32, _::_*8>>
  def ssh_string(data), do: <<byte_size(data)::unsigned-integer-32>> <> data

  @doc """
  Get the "key type" string for given public key, i.e. "ssh-ed25519".

  Only ED25519 keys are supported.
  """
  @spec pub_key_to_ssh_name(:public_key.public_key()) :: String.t()
  def pub_key_to_ssh_name(key)

  def pub_key_to_ssh_name({:ed_pub, :ed25519, _}), do: "ssh-ed25519"
  def pub_key_to_ssh_name(key), do: raise("Unknown key type #{inspect(key)}")

  @doc """
  Extract public key data bytes from key tuple.
  """
  @spec pub_key_extract(:public_key.public_key()) :: binary()
  def pub_key_extract(key)

  def pub_key_extract({:ed_pub, _, key}), do: key
  def pub_key_extract(key), do: raise("Unknown key type #{inspect(key)}")

  @doc """
  Get the OpenSSH signature start delimiter.
  """
  @spec openssh_sig_start() :: String.t()
  def openssh_sig_start(), do: @openssh_sig_start

  @doc """
  Get the OpenSSH signature end delimiter.
  """
  @spec openssh_sig_end() :: String.t()
  def openssh_sig_end(), do: @openssh_sig_end

  @spec parse_preamble(binary()) :: {:ok, binary()} | {:error, atom()}
  defp parse_preamble(blob)

  defp parse_preamble(<<
         @magic_preamble,
         sig_version::unsigned-integer-32,
         rest::binary
       >>)
       when sig_version == @sig_version,
       do: {:ok, rest}

  defp parse_preamble(<<
         @magic_preamble,
         _sig_version::unsigned-integer-32,
         _rest::binary
       >>),
       do: {:error, :version}

  defp parse_preamble(_), do: {:error, :preamble}

  @spec parse_keys(binary()) :: {:ok, [PublicKey.t()], binary()} | {:error, atom()}
  defp parse_keys(blob)

  defp parse_keys(<<
         key_blob_len::unsigned-integer-32,
         key_blob::binary-size(key_blob_len),
         rest::binary
       >>) do
    case :ssh_file.decode(key_blob, :ssh2_pubkey) do
      {:error, _} -> {:error, :keys}
      keys when is_list(keys) -> {:ok, Enum.map(keys, &%PublicKey{data: &1}), rest}
      single_key -> {:ok, [%PublicKey{data: single_key}], rest}
    end
  end

  defp parse_keys(_), do: {:error, :key}

  @spec parse_namespace(binary()) :: {:ok, binary(), binary()} | {:error, atom()}
  defp parse_namespace(blob)

  defp parse_namespace(<<
         namespace_len::unsigned-integer-32,
         namespace::binary-size(namespace_len),
         rest::binary
       >>),
       do: {:ok, namespace, rest}

  defp parse_namespace(_), do: {:error, :namespace}

  @spec parse_reserved(binary()) :: {:ok, binary()} | {:error, atom()}
  defp parse_reserved(blob)

  defp parse_reserved(<<
         reserved_len::unsigned-integer-32,
         _reserved::binary-size(reserved_len),
         rest::binary
       >>),
       do: {:ok, rest}

  defp parse_reserved(_), do: {:error, :reserved}

  @spec parse_hash(binary()) :: {:ok, hash_algo(), binary()} | {:error, atom()}
  defp parse_hash(blob)

  defp parse_hash(<<
         hash_len::unsigned-integer-32,
         hash::binary-size(hash_len),
         rest::binary
       >>)
       when hash in ["sha256", "sha512"],
       do: {:ok, String.to_existing_atom(hash), rest}

  defp parse_hash(_), do: {:error, :hash}

  @spec parse_signature_part(binary()) :: {:ok, binary()} | {:error, atom()}
  defp parse_signature_part(blob)

  defp parse_signature_part(<<
         signature_blob_len::unsigned-integer-32,
         signature_blob::binary-size(signature_blob_len)
       >>) do
    case signature_blob do
      <<
        key_type_len::unsigned-integer-32,
        _key_type::binary-size(key_type_len),
        signature_len::unsigned-integer-32,
        signature::binary-size(signature_len)
      >> ->
        {:ok, signature}

      _ ->
        {:error, :signature}
    end
  end

  defp parse_signature_part(_), do: {:error, :signature}
end
