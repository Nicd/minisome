defmodule Minisome.Some.Fetcher do
  import Minisome.Utils.TypedStruct

  alias Minisome.Storage.Auth.Host

  use GenServer

  defmodule Options do
    deftypedstruct(%{
      name: {GenServer.name(), Minisome.Some.Fetcher},
      fetch_delay: pos_integer(),
      hosts_getter: (() -> [Host.t()])
    })
  end

  defmodule State do
    deftypedstruct(%{
      fetch_delay: pos_integer(),
      hosts: [Host.t()],
      timer_ref: :timer.tref()
    })
  end

  @spec start_link(Options.t()) :: GenServer.on_start()
  def start_link(%Options{} = opts) do
    GenServer.start_link(__MODULE__, opts, name: opts.name)
  end

  @impl GenServer
  @spec init(Options.t()) :: {:ok, State.t()}
  def init(opts) do
    {:ok, tref} = :timer.send_after(opts.fetch_delay, self(), :fetch)

    hosts = opts.hosts_getter.()

    {:ok,
     %State{
       fetch_delay: opts.fetch_delay,
       hosts: hosts,
       timer_ref: tref
     }}
  end

  @impl GenServer
  @spec handle_info(any(), State.t()) :: {:noreply, State.t()}
  def handle_info(msg, state)

  def handle_info(:fetch, state) do
    feeds =
      for host <- state.hosts do
        Minisome.Some.HTTP.get(host, "/feed")
      end
      |> Enum.reject(&(&1 == :error))
      |> Enum.map(fn {:ok, msg} -> decode_feed(msg) end)
      |> Enum.reject(&(&1 == :error))

    Phoenix.PubSub.broadcast!(Minisome.Some.Fetcher.PubSub, "feed", {:feed, feeds})

    {:ok, tref} = :timer.send_after(state.fetch_delay, self(), :fetch)
    {:noreply, %State{state | timer_ref: tref}}
  end

  @spec decode_feed(Minisome.API.Client.MessageLibraries.Feed.FeedMessage.t()) ::
          Minisome.API.Client.MessageLibraries.Feed.FeedMessage.Decoded.t() | :error
  defp decode_feed(feed) do
    raw_contents = feed.contents

    with protocol_messages <-
           Enum.map(raw_contents, &Minisome.API.Client.ProtocolHandler.decode_raw/1),
         true <-
           Enum.all?(protocol_messages, fn
             {:ok, _} -> true
             _ -> false
           end),
         decoded_messages <-
           Enum.map(protocol_messages, fn {:ok, msg} ->
             Minisome.API.Client.ProtocolHandler.handle_message(msg)
           end) do
      %Minisome.API.Client.MessageLibraries.Feed.FeedMessage.Decoded{
        contents: decoded_messages |> Enum.reject(&(&1 == :error)) |> Enum.map(&elem(&1, 1))
      }
    else
      _ -> :error
    end
  end
end
