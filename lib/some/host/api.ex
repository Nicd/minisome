defmodule Minisome.Some.Host.API do
  @spec all(Ecto.Repo.t()) :: [Minisome.Storage.Auth.Host.t()]
  def all(repo \\ Minisome.Storage.Repo) do
    repo.all(Minisome.Storage.Auth.Host)
  end
end
