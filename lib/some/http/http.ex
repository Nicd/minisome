defmodule Minisome.Some.HTTP do
  alias Minisome.Storage.Auth.Host
  alias Minisome.API.Client.Message.RawMessage

  @spec get(Host.t(), String.t()) :: {:ok, RawMessage.t()} | :error
  def get(host, path) do
    req = build_req(host, path)

    with {:ok, resp} <- Finch.request(req, __MODULE__),
         msg when msg != nil <- Minisome.API.Client.Message.parse_message(resp.body),
         {:ok, proto_msg} <- Minisome.API.Client.ProtocolHandler.decode_raw(msg),
         {:ok, handled_msg} <-
           Minisome.API.Client.ProtocolHandler.handle_message(proto_msg) do
      {:ok, handled_msg}
    else
      _ -> :error
    end
  end

  @spec build_req(Host.t(), String.t()) :: Finch.Request.t()
  def build_req(host, path) do
    url = URI.merge(URI.new!("http://#{host.hostname}:#{host.port}"), %URI{path: path})

    Finch.build(:get, url, [{"accept", Application.get_env(:minisome, :mime_type)}])
  end
end
