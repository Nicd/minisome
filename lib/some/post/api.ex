defmodule Minisome.Some.Post.API do
  alias Ecto.Changeset
  alias Minisome.Storage.Repo
  alias Minisome.Storage.Some.Post

  @spec create_changeset(map()) :: Changeset.t()
  def create_changeset(params \\ %{}) do
    %Post{}
    |> Changeset.cast(params, [:text, :tags])
    |> Changeset.validate_required([:text])
    |> Changeset.validate_length(:text, min: 1)
    |> Changeset.validate_change(:tags, fn
      _, val when is_list(val) ->
        failed = Enum.reject(val, &String.valid?/1)

        if failed != [] do
          [tags: "Tags must be strings."]
        else
          []
        end

      _, _val ->
        [tags: "Tags need to be a list."]
    end)
    |> Changeset.put_change(:posted, DateTime.utc_now() |> DateTime.truncate(:second))
  end

  @spec create(Changeset.t(), Ecto.Repo.t()) :: {:ok, Post.t()} | {:error, Changeset.t()}
  def create(changeset, repo \\ Repo) do
    repo.insert(changeset)
  end

  @spec all(Ecto.Repo.t()) :: [Post.t()]
  def all(repo \\ Repo) do
    repo.all(Post)
  end
end
