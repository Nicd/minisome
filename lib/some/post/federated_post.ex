defmodule Minisome.Some.Post.FederatedPost do
  import Minisome.Utils.TypedStruct

  deftypedstruct(%{
    post: Minisome.API.Client.MessageLibraries.Post.PostMessage.t(),
    original: Minisome.API.Client.Message.RawMessage.t(),
    path: [Minisome.Storage.Auth.Host.t()]
  })
end
