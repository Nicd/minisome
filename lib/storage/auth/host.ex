defmodule Minisome.Storage.Auth.Host do
  use Ecto.Schema
  import Minisome.Storage.TypedSchema

  deftypedschema "hosts" do
    field(:hostname, :string, String.t())
    field(:port, :integer, pos_integer())

    has_many(:keys, Minisome.Storage.Auth.Key, [Minisome.Storage.Auth.Key.t()])
  end
end
