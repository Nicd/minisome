defmodule Minisome.Storage.Auth.Key do
  use Ecto.Schema
  import Minisome.Storage.TypedSchema

  deftypedschema "keys" do
    field(:key_blob, :string, String.t())
    field(:expires, :utc_datetime, DateTime.t())

    belongs_to(:host, Minisome.Storage.Auth.Host, Minisome.Storage.Auth.Host.t())
  end
end
