defmodule Minisome.Storage.Auth.MyKey do
  use Ecto.Schema
  import Ecto.Query, only: [from: 2]
  import Minisome.Storage.TypedSchema

  alias Minisome.Crypto.SSH

  deftypedschema "my_keys" do
    field(:key_blob, :string, String.t())
    field(:expires, :utc_datetime, DateTime.t())
  end

  @spec get_active_keys(Ecto.Repo.t()) :: [{SSH.KeyPair.t(), DateTime.t()}]
  def get_active_keys(repo \\ Minisome.Storage.Repo) do
    from(k in __MODULE__, where: k.expires >= ^DateTime.utc_now())
    |> repo.all()
    |> Enum.map(fn %__MODULE__{} = key ->
      {:ok, pair} = SSH.load_key_pair(key.key_blob)
      {pair, key.expires}
    end)
  end
end
