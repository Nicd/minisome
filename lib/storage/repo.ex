defmodule Minisome.Storage.Repo do
  use Ecto.Repo,
    otp_app: :minisome,
    adapter: Ecto.Adapters.SQLite3
end
