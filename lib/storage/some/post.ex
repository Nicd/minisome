defmodule Minisome.Storage.Some.Post do
  use Ecto.Schema
  import Minisome.Storage.TypedSchema

  deftypedschema "posts" do
    field(:posted, :utc_datetime, DateTime.t())
    field(:text, :string, String.t())
    field(:tags, {:array, :string}, [String.t()])
  end
end
