defmodule Minisome.Utils.WithHelper do
  @spec op(atom(), any(), :strict | :permissive) :: any()
  def op(label, thing, mode \\ :strict) do
    if mode == :permissive do
      opt_permissive(label, thing)
    else
      op_strict(label, thing)
    end
  end

  defp opt_permissive(label, err) when err in [:error, false, nil], do: {label, err}
  defp opt_permissive(label, {:error, _} = err), do: {label, err}
  defp opt_permissive(_label, val), do: val

  defp op_strict(_label, val) when val in [:ok, true], do: val
  defp op_strict(_label, {:ok, _} = success), do: success
  defp op_strict(label, other), do: {label, other}
end
