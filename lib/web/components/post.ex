defmodule Minisome.Web.Components.Post do
  import Phoenix.LiveView.Helpers, only: [sigil_H: 2]

  @spec my(%{post: Minisome.Storage.Some.Post.t()}) :: Phoenix.LiveView.Rendered.t()
  def my(assigns) do
    ~H"""
    <section class="post">
      <header>
        <p>ID: <%= @post.id %></p>
      </header>
      <p class="post-body">
        <%= @post.text %>
      </p>
      <footer>
        <p>Posted on <%= Calendar.strftime(@post.posted, "%c") %><%= if @post.tags in [nil, []] do "" else " with tags " <> Enum.join(@post.tags, ",") end %>.</p>
      </footer>
    </section>
    """
  end

  @spec message(%{post: Minisome.API.Client.MessageLibraries.Post.PostMessage.t()}) ::
          Phoenix.LiveView.Rendered.t()
  def message(assigns) do
    ~H"""
    <section class="post">
      <p class="post-body">
        <%= @post.text %>
      </p>
      <footer>
      <p>Posted on <%= Calendar.strftime(@post.posted, "%c") %><%= if @post.tags in [nil, []] do "" else " with tags " <> Enum.join(@post.tags, ",") end %>.</p>
      </footer>
    </section>
    """
  end
end
