defmodule Minisome.Web.PageController do
  use Minisome.Web, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
