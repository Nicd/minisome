defmodule Minisome.Web.Live.Feed do
  use Minisome.Web, :live_view

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    :ok = Phoenix.PubSub.subscribe(Minisome.Some.Fetcher.PubSub, "feed")

    {:ok, assign(socket, posts: [])}
  end

  @impl Phoenix.LiveView
  def handle_info(msg, socket)

  def handle_info({:feed, feeds}, socket) do
    posts =
      for feed <- feeds, reduce: [] do
        acc -> Enum.concat(feed.contents, acc)
      end
      |> Enum.sort_by(& &1.posted, {:desc, DateTime})

    {:noreply, assign(socket, posts: posts)}
  end
end
