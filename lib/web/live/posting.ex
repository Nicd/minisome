defmodule Minisome.Web.Live.Posting do
  use Minisome.Web, :live_view

  alias Minisome.Some.Post.API, as: PostAPI

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    {:ok,
     assign(
       socket,
       cset: PostAPI.create_changeset(),
       created: nil
     )}
  end

  @impl Phoenix.LiveView
  def handle_event(event, params, socket)

  def handle_event("change", params, socket) do
    {:noreply, assign(socket, cset: PostAPI.create_changeset(Map.get(params, "post", %{})))}
  end

  def handle_event("submit", params, socket) do
    cset = PostAPI.create_changeset(Map.get(params, "post", %{}))

    socket =
      case PostAPI.create(cset) do
        {:ok, post} ->
          assign(socket, created: post)

        {:error, error_cset} ->
          assign(socket, cset: error_cset)
      end

    {:noreply, socket}
  end
end
