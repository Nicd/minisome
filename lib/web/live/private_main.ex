defmodule Minisome.Web.Live.PrivateMain do
  use Minisome.Web, :live_view

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    posts = Minisome.Some.Post.API.all()
    {:ok, assign(socket, posts: posts)}
  end
end
