defmodule Minisome.Web.Live.Signing do
  use Minisome.Web, :live_view

  alias Minisome.Crypto.SSH
  alias Minisome.API.Client.Message

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    socket =
      assign(socket,
        keys: nil,
        input: "",
        signed: "",
        verified: false,
        verified_msg: ""
      )

    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_event(event, value, socket)

  def handle_event("set_key", %{"value" => key}, socket) do
    case SSH.load_key_pair(key) do
      {:ok, key_pair} ->
        {:noreply, assign(socket, keys: key_pair)}

      :error ->
        {:noreply, assign(socket, keys: nil)}
    end
  end

  def handle_event("set_input", %{"value" => input}, socket) do
    full_message =
      if not is_nil(socket.assigns.keys) do
        signature = SSH.sign(input, socket.assigns.keys)

        raw = %Message.RawMessage{data: input, signature: signature}
        Message.format_message(raw)
      else
        ""
      end

    parsed_message = Message.parse_message(full_message)

    verified =
      SSH.verify(parsed_message.data, parsed_message.signature, socket.assigns.keys.public) and
        parsed_message.data == input

    {:noreply,
     assign(socket,
       input: input,
       signed: full_message,
       verified: verified,
       verified_msg: parsed_message.data
     )}
  end
end
