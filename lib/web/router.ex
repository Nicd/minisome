defmodule Minisome.Web.Router do
  use Minisome.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {Minisome.Web.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", Minisome.Web do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/private", Minisome.Web do
    pipe_through :browser

    live "/", Live.PrivateMain
    live "/feed", Live.Feed
    live "/key-testing", Live.Signing
    live "/posting", Live.Posting
  end
end
