defmodule Minisome.Storage.Repo.Migrations.AddHosts do
  use Ecto.Migration

  def change do
    create table(:hosts) do
      add(:hostname, :text, null: false)
      add(:port, :integer, null: false)
    end

    create table(:keys) do
      add(:key_blob, :text, null: false)
      add(:expires, :utc_datetime, null: false)
    end
  end
end
