defmodule Minisome.Storage.Repo.Migrations.AddMyKeys do
  use Ecto.Migration

  def change do
    create table(:my_keys) do
      add(:key_blob, :text, null: false)
      add(:expires, :utc_datetime, null: false)
    end
  end
end
