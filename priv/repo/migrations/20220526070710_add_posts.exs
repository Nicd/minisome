defmodule Minisome.Storage.Repo.Migrations.AddPosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add(:posted, :utc_datetime, null: false)
      add(:text, :text, null: false)
      add(:tags, {:array, :string}, null: false, default: [])
    end
  end
end
